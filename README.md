**git is awesome. its the best source control.
i love git and the people who made it, they're awesome.
i am an elite programmer and i know how to use git.

git is a free and open source distributed revision control system.
git is easy to learn https://git-scm.com/documentation



תשובות לשאלות:

שאלה 1: זה מאפשר לקבוצת מתכנתים לעבוד בקבוצה על פרוייקט אחד ולעדכן אותו שצריכים, תוך כדי שמירה על סדר ונותן אפשרות למזג תשובות וקודים שונים על מנת שיצא קוד אחד נכון ואסתטי, תוך כדי שיתוף פעולה של כל חברי הקבוצה.

שאלה 2: כל עוד לא הוסיפו קבצים, אפשר להשתמש בgit diff
אם הוסיפו קבצים: git diff –cached
אם רוצים לבדוק שינויים מהגרסא שיש עכשיו בעץ העובד הנוכחי לבין הראשי:git diff HEAD

שאלה 3: git log

שאלה 4: git checkout commit-id

שאלה 5: rebase:

העברת ענף מקומיט אחד לקומיט אחר. זה טוב למקרה שבמקרה לקחת קומיט לא נכון, אתה יכול לעשות ריבייס ובכך תוכל "לעקוף" את הבעיה ותוכל לתקן את הטעות.

שאלה 6: svn log -v -r <revision>